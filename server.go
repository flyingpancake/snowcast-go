package main

import (
    "fmt"
    "net"
    "os"
    "encoding/binary"
    "bufio"
    "time"
    "sync"
    "io"
    "strconv"
)

const (
    // COMMUNICATION PROTOCOLS: CLIENT
    HELLO uint8 = 0
    SET_STATION uint8 = 1
    // COMMUNICATION PROTOCOLS: Server
    WELCOME uint8 = 0
    ANNOUNCE uint8 = 1
    INVALID_COMMAND uint8 = 2
)

type station struct {
    songName string
    mp3 *os.File
    mutex sync.RWMutex
    clientMap map[*net.Conn]*net.Conn
    // maps TCP connection socket pointers to UDP connection socket pointers
}

type client struct {
    // A client is allocated for each client that successfully completes the initial handshake protocol and setstation protocols
    tcpConn *net.Conn
    udpConn *net.Conn
}

var stationList []station
var numStations uint16

func main() {
    var waitGroup sync.WaitGroup // Initialize a waitgroup to terminate our server if our server is terminated from console
    // Command line usage: snowcast_server <tcpport> <file1> ... 
    if len(os.Args) < 3 {
        fmt.Fprintf(os.Stderr, "usage: snowcast_server <tcpport> <file1> ... \n")
        os.Exit(1)
    }
    // Initialize tcp socket listener
    ln, err := net.Listen("tcp", ":"+ os.Args[1])
    if err != nil {
        fmt.Println("Error listening to port", err.Error())
        os.Exit(1)
    }
    defer ln.Close() //Cleanup
    // Initialize music streaming stations: we have one per each file added to the server. Each station streams a single song in a loop, and all clients are synchronized with the station
    initializeStations() 
    waitGroup.Add(1)
    // Handle standard input in new routine to not block the main routine
    go handleStdin(&waitGroup)
    // Thread handling all new connections to the server in a nonblocking manner
    go acceptNewConnections(&ln)
    // To ensure our program does not terminate immediately after initiating the threads, we wait on termination sequence from standard input
    waitGroup.Wait()
}

func initializeStations() {
    fmt.Println("> Initializing Radio Stations...")
    // The number of stations is the number of files entered in the command line 
    numStations = uint16(len(os.Args) - 2)
    stationList = make([]station, numStations)
    for idx, elt := range os.Args[2:] {
        filePtr, err := os.Open(elt)
        if err != nil {
            fmt.Printf("Open(%s), %v\n", elt, err.Error())
            os.Exit(1) // An invalid file location was sent and the server must terminate
        }
        // Per station, we retain:
        // 1. A file pointer to the song it is playing
        // 2. It's name as an ASCII encoded string
        // 3. A list of all clients, formatted as the client's tcp socket pointer, and the UDP socket pointer to which songs are streamed (decided by client)
        stationList[idx].mp3 = filePtr 
        stationList[idx].songName = elt
        stationList[idx].clientMap = make(map[*net.Conn]*net.Conn)
        // Initialize a thread to play the songs of each station in nonblocking style
        go (&stationList[idx]).playSong()
    }
}

// Server accepts new stations using this thread
func acceptNewConnections(ln *net.Listener){
    for {
        conn, err := (*ln).Accept()
        if err != nil {
        fmt.Println("> Error accepting connection")
        }
        go handleTCP(&conn) // Handle TCP deals with main logic for any any further communication over TCP socket. We only accept connections in this function
    }
}

// Handle TCP connection logic between server-client
func handleTCP(conn *net.Conn){
    defer (*conn).Close() // cleanup
    // For synchronization purposes, to ensure that the handshake protocol is executed properly
    var receivedHello bool = false
    var prevStation int = -1
    var udpPort uint16
    clientAddr, _, err := net.SplitHostPort((*conn).RemoteAddr().String())  // Parse client address from socket connection object
    if clientAddr == "::1" {
        clientAddr = "localhost"
    }
    if err != nil {
        fmt.Println(err.Error()) // This error is rare and will occur in the unlikely event that the connection object is terminated in the sequence between starting the socket and parsing client address
        return // We must terminate in this condition
    }
    fmt.Println("New client connected from:", clientAddr)
    // Message buffer to hold handshake protocol bytes in network order
    msgBuffer := make([]byte, 3)
    // If we terminate the client connection, an informative message is printed
    defer fmt.Println("> Killed connection to", clientAddr)
    for {
        // Readbuffer handles reading the entire byte sequence from the buffer - EVEN if somehow the network delays later bytes. We only block for a finite amount of time before timing out (see function)
        msgBuffer, err := readBuffer(0, 3, msgBuffer, conn) 
        if err != nil {
            // If timeout, print error message, delete client from stremaing service, and return
            if timeoutError, ok := err.(net.Error); ok && timeoutError.Timeout() {
                fmt.Println("> Client timed out: remaining bytes of message not received.")
            } else {
                // Non timeout-related error, print error and terminate
                fmt.Println("> Client closed connection |", err.Error())
            }
            // Client is deleted so that the streaming thread will not see it
            deleteClient(prevStation, conn)
            return
        }
        switch msgBuffer[0]{
            // We act depending on what the initial byte decrees: allowed messages are HELLO, SET_STATION. anything else results in error
            case HELLO: 
                if receivedHello { 
                    // This is invalid use of the protocol: if the client attempts another handshake after the initial handshake is successful
                    sendAnnounceOrInvalid(conn, INVALID_COMMAND, "Server already received HELLO. Terminating connection")
                    deleteClient(prevStation, conn)
                    return
                }
                // A boolean to ensure protocol correctness (handshake)
                receivedHello = true
                // Read port number from network byte order that the client sends
                udpPort = binary.BigEndian.Uint16(msgBuffer[1:])
                err := sendWelcome(conn)
                if err != nil {
                    return // If the error here is non-nil, there must be some network error over the connection object that the server cannot handle. The client connection is terminated
                }
            case SET_STATION:
                newStation := binary.BigEndian.Uint16(msgBuffer[1:])
                //Protocol correctness check
                if !receivedHello {
                    sendAnnounceOrInvalid(conn, INVALID_COMMAND, "Client must send HELLO before SET_STATION. Terminating...")
                    return
                }
                // Protocol correctness check
                if (newStation < 0) || (newStation >= numStations) {
                    sendAnnounceOrInvalid(conn, INVALID_COMMAND, "Server received a SET_STATION command with an invalid station number")
                    deleteClient(prevStation, conn)
                    return
                } else if prevStation == -1 { 
                    //If prevStation is -1 (default value), then this is the client's first time setting a station
                    prevStation = int(newStation)
                    // We dial the udpConnection as this is the client's first time setting a station
                    udpCxn, err := net.Dial("udp",clientAddr+":"+ strconv.Itoa(int(udpPort))) 
                    if err != nil {
                        // If there is an error here, the server cannot handle it and it is the fault of the udpPort sent by the client
                        fmt.Println(err.Error())
                        return
                    }
                    // Stationlist synchronization primitives, in case of race conditions
                    stationList[prevStation].mutex.Lock()
                    stationList[prevStation].clientMap[conn] = &udpCxn
                    stationList[prevStation].mutex.Unlock()
                    // Announce that the station has been set, to the respective client
                    sendAnnounceOrInvalid(conn, ANNOUNCE, stationList[prevStation].songName)
                } else {
                    // If the new setstation request does not change the station, we're in luck and do nothing! Otherwise...
                    if prevStation != int(newStation){
                        // Synchronization to ensure no race conditions
                        stationList[prevStation].mutex.Lock()
                        udpCxn := stationList[prevStation].clientMap[conn]
                        delete(stationList[prevStation].clientMap, conn) 
                        stationList[prevStation].mutex.Unlock()
                        prevStation = int(newStation)
                        stationList[prevStation].mutex.Lock()
                        stationList[prevStation].clientMap[conn] = udpCxn
                        stationList[prevStation].mutex.Unlock()
                    }
                    // An announce is sent to indicate that the station has changed
                    sendAnnounceOrInvalid(conn, ANNOUNCE, stationList[prevStation].songName)
                }
                fmt.Printf("> [Client %s] | Received SET_STATION command to station: %d\n", clientAddr, prevStation)
            default: 
                // Any message other than set_station and hello is invalid
                if !receivedHello {
                    sendAnnounceOrInvalid(conn, INVALID_COMMAND, "Client must send HELLO before further communication, terminating client")
                    return
                }
                sendAnnounceOrInvalid(conn, INVALID_COMMAND, "Invalid Command type")
                deleteClient(prevStation, conn)
                return
        }
    }
}

// Simple function to delete a client. It checks first to ensure that a station has indeed been set
func deleteClient(station int, conn *net.Conn) {
    if station != -1 {
        stationList[station].mutex.Lock()
        delete(stationList[station].clientMap, conn)
        stationList[station].mutex.Unlock()
    }
}

// Function abstracting the welcome protocol
func sendWelcome(conn *net.Conn) error {
    fmt.Println("> HELLO received; sending WELCOME; expecting SET_STATION")
    buf := make([]byte, 3)
    buf[0] = WELCOME
    binary.BigEndian.PutUint16(buf[1:3], numStations)
    _, err := (*conn).Write(buf)
    if err != nil {
        fmt.Println("> Error sending WELCOME to client |", err.Error())
        return err
    }
    return nil
}

func sendAnnounceOrInvalid(conn *net.Conn, cmdType uint8, cmdString string) {
    if cmdType == INVALID_COMMAND {
        fmt.Println("> Invalid Client protocol, terminating connection...")
    }
    strSize := uint8(len(cmdString))
    buf := make([]byte, 2 + strSize)
    buf[0] = cmdType
    buf[1] = strSize
    copy(buf[2:], cmdString)
    _, err := (*conn).Write(buf)
    if err != nil {
        fmt.Println("> Error writing to client", err.Error())
    }
}

// Thread that handles playing the song for a station routine. It streams the song to all available clients
func (stn *station) playSong(){
    songFile := stn.mp3
    defer func(){
            // Cleanup functions, we don't defer close because we need to see the error, if any, met upon closing
            err := songFile.Close()
            if err != nil {
                fmt.Println("> Close() Error |", err.Error())
            }
        }()
    // A pointer to where we are seeking in the file
    position := 0
    buf := make([]byte, 1500) // Buffer size that mirrors UDP packet size
    for {
        bytesRead, err := songFile.ReadAt(buf, int64(position)) // Seeks to a specific point in the songFile on disk
        if err != nil {
            if err == io.EOF { // if end of file, we loop around and inform all clients
                stn.sendAnnounce()
                // stream the bytes read up to End of File
                stn.streamUDP(buf[:bytesRead]) 
                position = 0 //reset seek pointer
            } else {
                fmt.Println("> Error reading file. ", err.Error())
            }
        } else {
            // Send the UDP packets to client UDP connections
            stn.streamUDP(buf[:bytesRead])
            position += bytesRead
        }
        // Synchronization step to ensure constant bitrate of mp3 (otherwise it would be completely garbled)
        time.Sleep(time.Microsecond * 91400)
    }
}

// helper to stream all bytes of data to all clients connected to a station
func (stn *station) streamUDP(data []byte) {
    stn.mutex.RLock() // ensure that nobody deletes clients as we iterate
    defer stn.mutex.RUnlock()
    for _, udpConn := range stn.clientMap {
        _, err := (*udpConn).Write(data)
        if err != nil {
            continue
        }
    }
}

// Helper that abstracts the sending of ANNOUNCE
func (stn *station) sendAnnounce(){
    stn.mutex.RLock()
    defer stn.mutex.RUnlock()
    for tcpConn, _ := range stn.clientMap {
        sendAnnounceOrInvalid(tcpConn, ANNOUNCE, stn.songName)
    }

}

// This is the timeout functionality we want in the event that there is some marginal network delay after sending the initial 
// contents of the message. We allow some amount of leeway before termination
func setTimeout(conn *net.Conn, duration int) {
    timeout := time.Duration(duration)*time.Millisecond
    (*conn).SetReadDeadline(time.Now().Add(timeout))
}

// A helper function that attempts to read the full numBytes (that the protocol) specifies into the buffer, in the event of network delay.
// If the bytes don't reach us before timeout, we have to terminate the client
func readBuffer(start int, numBytes int, buff []byte, conn *net.Conn) ([]byte, error){
    defer (*conn).SetReadDeadline(time.Time{})
    lengthRead := 0
    for lengthRead < numBytes {
        l, err := (*conn).Read(buff[lengthRead + start:numBytes])
        if err != nil {
            return buff, err
        } 
        lengthRead += l
        // 125 ms timeout
        setTimeout(conn, 125)
    }
    return buff, nil
}

func handleStdin(waitGroup *sync.WaitGroup) {
    // Simple CLI interface for the server
    fmt.Println("> Type 'q' or 'quit' to EXIT; 'ls' or 'list' to LIST all playing songs.")
    // A waitgroup for synchronization purposes
    defer (*waitGroup).Done()
    scanner := bufio.NewScanner(os.Stdin)
    for {
        if scanner.Scan() == false {
            // if CTRL-C or other signal, return
            return
        }
        str := scanner.Text()
        if (str == "q") || (str == "quit") {
            fmt.Println("> Terminating server...")
            return
        } else if (str == "ls") || (str == "list") {
            fmt.Println("> Listing all playing songs...") 
            listSongs()
        } else {
            fmt.Println("> Invalid command, type 'q' or 'quit' to quit server")
        }
    }
}

// simple function to list all songs
func listSongs() {
    for i, v := range stationList {
        fmt.Printf("%d. %s\n", i ,v.songName)
    }
}