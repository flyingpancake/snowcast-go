package main

import (
    "fmt"
    "os"
    "encoding/binary"
    "strconv"
    "net"
    "sync"
    "time"
    "bufio"
)

var welcomeReceived bool
var helloSent bool
var setStation bool

var helloLock sync.Mutex
var stationLock sync.Mutex

const (
    //client-to-server messages
    HELLO uint8 = 0  
    SET_STATION uint8 = 1
    //server-to-client replies
    WELCOME uint8 = 0 
    ANNOUNCE uint8 = 1
    INVALID_COMMAND uint8 = 2
)

func main() {
    // Waitgroup to synchronize main client routine: if client is terminated from STDIN, we terminate. If client is terminated from server, we also terminate
    var waitGroup sync.WaitGroup 
    if len(os.Args) != 4 {
        fmt.Fprintf(os.Stderr, "Usage: snowcast_control <servername> <serverport> <udpport> \n")
        os.Exit(1)
    }
    // parse server information from command line
    serverName, serverPort := os.Args[1], os.Args[2]
    udpPort, err := strconv.Atoi(os.Args[3])
    if err != nil {
        fmt.Println("<udpport> invalid syntax")
        os.Exit(1)
    }
    // dial server to get tcp connection socket pointer
    conn, err := net.Dial("tcp", serverName + ":" + serverPort)
    if err != nil {
        fmt.Println(err.Error())
        os.Exit(1)
    }
    fmt.Printf("Connected to server %s from %s\n", conn.RemoteAddr().String(), conn.LocalAddr().String())
    waitGroup.Add(1)  // Wait for one of the two goroutines to terminate - if so main terminates
    go handleStdin(&conn, &waitGroup) //thread handling standard input
    fmt.Println("Type in 'q', 'quit', or press CTRL+C to quit")
    go handleConnection(&conn, &waitGroup) //thread handling TCP socket connection
    err = sendCmd(&conn, HELLO, uint16(udpPort))  // send HELLO protocol to server
    if err != nil {
        // Any error writing to the server is not the fault of the client - there's not much we can do besides terminate
        fmt.Println("Error writing to server connection.", err.Error())
        os.Exit(1)
    }
    waitGroup.Wait() // main blocks until one of the two above threads/routines terminates
}
// sends either HELLO or SETSTATION commands
func sendCmd(conn *net.Conn, cmdType uint8, portNumberOrStation uint16) error {
    if cmdType == HELLO {
        helloLock.Lock() // make sure no race conditions on helloSent boolean (we have two threads handling this boolean)
        helloSent = true
        helloLock.Unlock()
        fmt.Println("Waiting for WELCOME...")
    } else if cmdType == SET_STATION {
        stationLock.Lock()
        setStation = true // make sure no race conditions on helloSent boolean
        stationLock.Unlock()
        fmt.Println("Waiting for ANNOUNCE...")
    }
    buf := make([]byte, 3)
    buf[0] = cmdType
    binary.BigEndian.PutUint16(buf[1:3], portNumberOrStation) // write in network byte order
    _ , err := (*conn).Write(buf)
    if err != nil {
        return err
    }  
    return nil
}

func handleConnection (conn *net.Conn, waitGroup *sync.WaitGroup){
    defer (*conn).Close()
    defer waitGroup.Done() //when connection terminates, we decrement so main stops blocking and program terminates
    msgBuffer := make([]byte, 512)
    for {
        // Listen on the connection and attempt to read server replies
        numBytes, err := (*conn).Read(msgBuffer)
        if err != nil {
            // If non-nil error, we do not know whether the server terminated the connection or some other serious connection error. In any event, we terminate
            fmt.Println("Server terminated the connection")
            return
        }
        // Further actions depend on the type of the message: Welcome, Announce, Invalid command, or some command that is not any of the previous and is invalid
        switch msgBuffer[0] {
            case WELCOME:
                // ensure correctness of handshake protocol: we don't want more than one WELCOME from the server
                if welcomeReceived {
                    fmt.Println("Invalid server protocol: more than one WELCOME received. Exiting...")
                    return
                } 
                // synchronization to ensure no race conditions on HELLOSENT
                helloLock.Lock()
                if !helloSent {
                    fmt.Println("Invalid server protocol: WELCOME received before HELLO sent. Exiting...")
                    helloLock.Unlock()
                    os.Exit(1)
                }
                helloLock.Unlock()
                // If for some reason we could not read the entire length of the message, attempt to read the rest, but time out if we cannot in a timely fashion
                if numBytes < 3 {
                    setTimeout(conn, 100)
                    msgBuffer, err = readBuffer(1, 2, msgBuffer, conn, true)
                    if err != nil {
                        fmt.Println(err.Error())
                        fmt.Println("TIMEOUT: Remaining bytes of message not received. Exiting...")
                        return
                    }
                }
                // We've properly received the welcome. Indicate this using a boolean
                welcomeReceived = true
                fmt.Println("WELCOME received. Handshake completed")
                fmt.Printf("Enter a STATION_NUMBER between 0 and %d \n", binary.BigEndian.Uint16(msgBuffer[1:3]) - 1)
            case ANNOUNCE:
                // station lock synchronizations the setStation variable to ensure no race conditions. WE want to make sure that the server sends ANNOUNCE only after a client sends a request to change station
                stationLock.Lock()
                if !setStation {
                    fmt.Println("ANNOUNCE message sent before SET_STATION, terminating.")
                    stationLock.Unlock()
                    return
                }
                stationLock.Unlock()
                // 120ms timeout to read the buffer
                setTimeout(conn, 120)
                msgBuffer, err := readAnnounceOrInvalid(conn, msgBuffer, numBytes)
                if err != nil {
                    fmt.Println("TIMEOUT: Remaining bytes of message not received. Exiting...")
                }
                msgSize := msgBuffer[1]
                fmt.Printf("New song announced: %s \n", msgBuffer[2:msgSize+2])
            case INVALID_COMMAND:
                // If invalid, we try to read the entire message to determine why our command sent weas invalid
                setTimeout(conn, 120)
                msgBuffer, err := readAnnounceOrInvalid(conn, msgBuffer, numBytes)
                if err != nil {
                    fmt.Println(err.Error())
                    fmt.Println("TIMEOUT: Remaining bytes of message not received. Exiting...")
                    return
                }
                msgSize := msgBuffer[1]
                fmt.Printf("INVALID_COMMAND_REPLY: %s. Terminating...\n", msgBuffer[2:msgSize+2])
                return
            default:
                // Unknown command, terminate
                fmt.Println(msgBuffer[0])
                fmt.Println("Invalid server protocol: unknown command. Exiting...")
                return
        }
    }
}

// Timeout on client-server connection in the event of network delays
func setTimeout(conn *net.Conn, duration int) {
    timeout := time.Duration(duration)*time.Millisecond
    (*conn).SetReadDeadline(time.Now().Add(timeout))
}

// Helper function that reads the entire byte sequence as specified by the initial bytes of an ANNOUNCE or INVALID
func readAnnounceOrInvalid(conn *net.Conn, buff []byte, numBytes int) ([]byte, error) {
    if numBytes < 2 {
        // If we've only read ONE byte (the byte specifiying the type of message, we need to read the second (specifying the message length)
        // We cannot make assumptions about the length of the message.
        buff, err := readBuffer(1, 1, buff, conn, false) // read a single byte into the buffer
        if err != nil {
            return buff, err
        }
        numBytes += 1
    }
    // Otherwise, if we're able to read the first two bytes, we know the message size
    msgSize := buff[1]
    if numBytes - 2 < int(msgSize) { // If the entire message was not read in one go (network delays, etc.)
        // Read the remainder of the message into the byffer
        buff, err := readBuffer(numBytes, int(msgSize) - (numBytes - 2), buff, conn, true)
        if err != nil {
            return buff, err
        }
    }
    // Reset the read deadline
    (*conn).SetReadDeadline(time.Time{})
    return buff, nil
}


// reads numBytes into buff, beginning with index start (inclusive)
func readBuffer(start int, numBytes int, buff []byte, conn *net.Conn, reset bool) ([]byte, error){
    if reset {
        defer (*conn).SetReadDeadline(time.Time{}) // reset timeout
    }
    lengthRead := 0
    for lengthRead < numBytes {
        l, err := (*conn).Read(buff[lengthRead + start: start + numBytes])
        if err != nil {
            return buff, err
        } 
        lengthRead += l
    }
    return buff, nil
}
// Routine to handle all input from Command Line
func handleStdin(conn *net.Conn, waitGroup *sync.WaitGroup) {
    defer waitGroup.Done()
    scanner := bufio.NewScanner(os.Stdin)
    for {
        if scanner.Scan() == false {
            return
        }
        str := scanner.Text()
        if str == "quit" || str == "q" {
            return
        } 
        n, err := strconv.Atoi(str)
        if err != nil {
            fmt.Println("Invalid input: expected 'quit', 'q', or integer")
            continue
        }
        err = sendCmd(conn, SET_STATION, uint16(n))
        if err != nil { 
            return
        }
    }
}