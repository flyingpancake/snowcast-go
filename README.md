# README #

### What is this repository for? ###

* Quick summary: highly concurrent internet radio station designed to handle hundreds of clients!
* Follows the snowcast protocol, layered over TCP for client-server communication
* Don't worry too much about system resources! We don't read song files straight into memory - they are chunked instead.
* Each client connection doesn't take more than 2MB of main memory! Connect as many as you want!
* Stream mp3 uninterrupted while synchronizing across all your favorite stations!

### How do I get set up? ###

* Summary of set up: Ensure that you have go 1.7 or newer on your system
* To build the server: go build server.go
* To build the client: go build client.go
* To build the listener: go build listener.go

### How do I run the server? ###

* Server command line syntax is: ./server tcpport file1 file2 ... 
* The tcp port number is the tcp port you listen for client connections on. The files are mp3 files you wish to stream - each is mapped to a station
* I've provided a series of mp3 songs in the repository, for convenience!

* Client command line syntax is: ./control servername serverport udpport
* Servername is either the machine ip address or some other translatable address (such as "localhost", if server and client are on the same machine)
* server port is the TCP port the server is listening on
* udpport is the udp port to which you want music to be streamed (on the same machine, of course!)

* Listener command line syntax: ./listener udpport
* listens on whichever udpport you specify! Make sure it is not a system port that is not authorized for application use

### Why does the listener only print to console? ###
* The listener is only a simple utility to read from a UDP Port. It does not handle mp3 decoding!
* To actually hear music, use your favorite mp3 decoding program (one easy way is the command "./listener [portNumber] | mpg123 -", if you have mpg123 installed) . 