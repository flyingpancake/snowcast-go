package main

import (
    "fmt"
    "net"
    "os"
)


func main() {
    // Command line arguments: snowcast_listener <udpPort>
    argc := len(os.Args)
    if argc != 2 {
        fmt.Println("snowcast_listener <udpPort>")
        os.Exit(1)
    }
    port := os.Args[1]
    // Resolve UDPPort number into net package readable address
    UDPAddr, err := net.ResolveUDPAddr("udp", ":" + port)
    if err != nil {
        fmt.Println("ResolveUDP error:", err.Error())
        os.Exit(1)
    }
    // Setup socket connection to UDP address (on the same host machine)
    udpConnPtr, err := net.ListenUDP("udp", UDPAddr)
    if err != nil {
        fmt.Println("Listen error:", err.Error())
        os.Exit(1)
    }
    // Create a 1500 byte buffer space to hold UDP packets, and print them to console
    buf := make([]byte, 1500)
    for {
        numBytes, err := udpConnPtr.Read(buf)
        if err != nil {
          fmt.Println("Read error:", err.Error())
        }
        fmt.Printf("%s", buf[:numBytes])
    }

}